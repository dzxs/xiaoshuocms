# from django.db import models
from mongoengine import *
from mongoengine import signals
import datetime
# Create your models here.
log = []


class Book(Document):
    book_id = IntField(required=True, unique=True)
    book_title = StringField(max_length=200)
    pic_url = URLField(max_length=200)
    category = StringField(max_length=50)
    author = StringField(max_length=50)

    @classmethod
    def post_save(cls, sender, document, **kwargs):
        if 'created' in kwargs:
            if kwargs['created']:
                log.append("Created ID: %d book: %s" % (document.book_id, document.book_title))
            else:
                log.append("Updated ID: %d book: %s" % (document.book_id, document.book_title))


class Page(Document):
    book_id = IntField(required=True)
    page_id = IntField(required=True)

    book = ReferenceField(Book)
    date = DateTimeField(default=datetime.datetime.now)
    page_title = StringField(max_length=200, required=True)
    page_url = URLField(max_length=200, unique=True)
    content = StringField()

    @classmethod
    def post_save(cls, sender, document, **kwargs):
        if 'created' in kwargs:
            if kwargs['created']:
                log.append("Created ID: %d page: %s" % (document.page_id, document.page_title))
            else:
                log.append("Updated ID: %d page: %s" % (document.page_id, document.page_title))

signals.post_save.connect(Book.post_save, sender=Book)
signals.post_save.connect(Page.post_save, sender=Page)