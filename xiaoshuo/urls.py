from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

from django.conf import settings

urlpatterns = patterns('',
    url(r'^$', 'web.views.home', name='home'),
    url(r'^crawl/$', 'web.views.crawl', name='crawl'),
    url(r'^checks/$', 'web.views.checks', name='checks'),
    url(r'^debug/$', 'web.views.debug', name='debug'),
    # url(r'^admin/$', include(admin.site.urls)),
    url(r'^admin/$', 'web.views.admin', name='admin'),
    url(r'^check_log/$', 'web.views.check_log', name='check_log'),
    url(r'^book/(?P<book_id>\d{1,4})/$', 'web.views.book', name='book'),
    url(r'^book/(?P<book_id>\d{1,4})/(?P<page_id>\d+)/$', 'web.views.page', name='page'),

    url(r'^baidu_verify_d9c0islUCd.html', 'web.views.baidu_verify_d9c0islUCd', name='baidu_verify'),
)


urlpatterns += patterns('',
   (r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATICFILES_DIRS[0]}),
)
